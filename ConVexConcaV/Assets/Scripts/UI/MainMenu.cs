﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button startButton;
    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(TaskOnClick);

    }

    void Update() {
        if (Input.GetButtonDown("Submit")) {
            TaskOnClick();
        }
    }


    void TaskOnClick()
    {
        // Debug.Log("Click");
        SceneManager.LoadScene("MainScene");
    }
}
