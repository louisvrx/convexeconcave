﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioManager : MonoBehaviour
{
    // Start is called before the first frame update

    public string SceneName;
    public AudioSource source_music;
    public AudioClip music_lvl1;
    public AudioClip music_lvl2;
    public AudioClip music_lvl3;
    public AudioClip music_lvl4;

    void Start()
    {        
        if (SceneName == "") {
            Debug.Log("Insert Scene Name !");
        } else if (SceneName == "MainScene" ) {
            source_music.clip = music_lvl1;
        } else if (SceneName == "MultiScreenScene" ) {
            source_music.clip = music_lvl2;
        } else if (SceneName == "DavidScene" ) {
            source_music.clip = music_lvl3;
        } 
        else if (SceneName == "RailScene" ) {
            source_music.clip = music_lvl4;
        } 
        
        source_music.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
