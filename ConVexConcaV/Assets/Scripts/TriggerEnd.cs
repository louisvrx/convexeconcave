﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerEnd : MonoBehaviour
{
    public GameObject endSceneCanvas;
    public string nextScene;
    Animator endSceneAnimator;
    Animator endSceneTextAnimator;
    Animator endSceneTextButtonAnimator;
    bool endTriggered;

    public AudioSource levelCompleteSound;
    public AudioSource musicSource;

    private static int characterExited;

    private void Start()
    {
        endSceneAnimator = endSceneCanvas.transform.Find("Panel").GetComponent<Animator>();
        endSceneTextAnimator = endSceneCanvas.transform.Find("Text").GetComponent<Animator>();
        endSceneTextButtonAnimator = endSceneCanvas.transform.Find("TextButtonAction").GetComponent<Animator>();
    }


    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log(other.gameObject);
        if (other.gameObject.tag == "Player") {
            characterExited++;
            StartCoroutine("FadeOutMusic");
            levelCompleteSound.Play();
        }
            
        if (characterExited == GameObject.FindGameObjectsWithTag("Player").Length)
        {
            endSceneAnimator.SetTrigger("end");
            endSceneTextAnimator.SetTrigger("end");
            endSceneTextButtonAnimator.SetTrigger("end");
            endTriggered = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Debug.Log(other.gameObject);
        if (other.gameObject.tag == "Player")
            characterExited--;
    }

    void Update()
    {
        if (endTriggered && Input.GetButtonDown("Submit"))
            StartCoroutine(TransitionNextScene(nextScene));
    }

    IEnumerator TransitionNextScene(string sceneName)
    {
        //endSceneAnimator.SetTrigger("end");
        endSceneTextAnimator.SetTrigger("hide");
        endSceneTextButtonAnimator.SetTrigger("hide");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }


    public IEnumerator FadeOutMusic() {
         for (float vol = musicSource.volume ; vol >= 0.0f ; vol = vol-0.05f) {
                    musicSource.volume = vol;
                }
        yield return new WaitForSeconds(0.05f);
    }

}
