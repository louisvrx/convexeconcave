﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum RayColor
{
    White,
    Red,
    Green,
    Blue
}

public class TestRay : MonoBehaviour
{
    public RayColor rayColor;

    [SerializeField]
    private Animator m_Animator;

    // doors sound
    private bool doors_opened;

    private static HashSet<int> emittersHit;
    private int nbOfEmitters;

    // Start is called before the first frame update
    void Start()
    {
        emittersHit = new HashSet<int>();
        nbOfEmitters = GameObject.FindGameObjectsWithTag("RayReceiver_White").Length;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("_____________emit_______"+ rayColor+"________");
        RaycastHit hit;
        Vector3 dir = transform.TransformDirection(Vector3.forward);
        List<Vector3> points = new List<Vector3>();
        points.Add(transform.position);
        bool hitThing = false;
        while(hitThing = Physics.Raycast(points[points.Count - 1], dir, out hit, 80, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Collide))
        {              
            points.Add(hit.point);       
            int layerHit = hit.transform.gameObject.layer;
            //Debug.Log("hit"+LayerMask.LayerToName(layerHit));
            //Debug.Log(hit.transform.name);
            if (layerHit == LayerMask.NameToLayer("Mirror") || layerHit == LayerMask.NameToLayer("Mirror_" + rayColor.ToString()))
            {
            //Debug.Log("hit");
                Vector3 bounce = Vector3.Reflect(dir, hit.normal);   
                Debug.DrawRay(hit.point, bounce, Color.red);
                dir = bounce;
                if (m_Animator.GetBool("IsOpen")) {
                    m_Animator.SetBool("IsOpen", false);
                }
            }
            else if(layerHit == LayerMask.NameToLayer("RayReceiver_" + rayColor.ToString())){
                emittersHit.Add(hit.transform.gameObject.GetInstanceID());
                Debug.Log("Reach goal"+ emittersHit.Count + " sss" + nbOfEmitters);
                if (emittersHit.Count == nbOfEmitters)
                {
                Debug.Log("Reach goal"+ m_Animator.name);
                    if (!m_Animator.GetBool("IsOpen"))
                    {
                        m_Animator.SetBool("IsOpen", true);
                        play_door_open();
                    }
                }
                break;
            }else {
                emittersHit.Remove(hit.transform.gameObject.GetInstanceID());
                if (m_Animator.GetBool("IsOpen")) {
                    m_Animator.SetBool("IsOpen", false);
                }
                break;
            }
                    
        }
        if(!hitThing)
        {
            points.Add(dir * 100);
        }
        
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.green);
        transform.GetComponent<LineRenderer>().positionCount =points.Count;
        transform.GetComponent<LineRenderer>().SetPositions(points.ToArray());
    }


    private void play_door_open() {
        AudioSource audio_source = GameObject.Find("Sound_DoorOpen").GetComponent<AudioSource>();
        if (!audio_source.isPlaying && !doors_opened ) {
            // Debug.Log("Play SOUND DOOR");
            audio_source.Play();
            doors_opened = true;
        }
    }

}
