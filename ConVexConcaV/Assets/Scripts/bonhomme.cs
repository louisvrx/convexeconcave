﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonhomme : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera mainCamera;
    public CharacterController _controller;
    public float speed = 0.1f;
    private bool grabbing;
    public int player_id;

    void Start()
    {
        grabbing = false;
        _controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotate = mainCamera.transform.rotation;
        Vector3 direction = new Vector3(Input.GetAxis ("side_p"+player_id), 0, Input.GetAxis ("front_p"+player_id));
        Debug.DrawRay(transform.position,direction, Color.red);
        if( direction.magnitude > 0.1f) {
            transform.LookAt(transform.position + direction);
            Vector3 move_direction = Vector3.forward*speed * direction.magnitude;
            _controller.Move(transform.localToWorldMatrix * move_direction);
            //transform.Translate(Vector3.forward*speed * direction.magnitude);
        }
        // if (Input.GetAxis ("grab") == 0.0f) {
        //     grabbing = false;
        // } else {
        //     grabbing = true;
        // }

        // if (grabbing == false) {
        //     transform.Translate(Vector3.forward * Input.GetAxis ("front")*0.1f);
        //     transform.Translate(Vector3.forward * Input.GetAxis ("front_p"+player_id)*0.1f);

        //     transform.Rotate(Vector3.up * Input.GetAxis ("side")*5f);
        //     transform.Rotate(Vector3.up * Input.GetAxis ("side_p"+player_id)*5f);
        //} 
    }

    // void OnCollisionEnter(Collision collision)
    // {
    //     //Output the Collider's GameObject's name
    //     Debug.Log(collision.collider.name);
    // }

    void OnCollisionStay(Collision collision)
    {
        //Check to see if the Collider's name is "Chest"
        if (collision.collider.name == "Mirror" && grabbing)
        {
            //Output the message
            // Debug.Log("Mirror is here!");
            collision.gameObject.transform.Rotate(Vector3.right *  Input.GetAxis ("side"));
            collision.gameObject.transform.Rotate(Vector3.right *  Input.GetAxis ("side_p"+player_id));
            collision.gameObject.transform.Rotate(Vector3.forward *  Input.GetAxis ("front"));
            collision.gameObject.transform.Rotate(Vector3.forward *  Input.GetAxis ("front_p"+player_id));
        }
    }
}
