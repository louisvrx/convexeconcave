﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{

    private bool grabbing;
    private bool grab_button;
    public int player_id;

    private float minimum_speed;
    public float sensibility = 0.2f;

    private CharacterController cc;
    private Animator m_Animator;
    private AudioSource[] audio_sources;
    private AudioSource grab_source;
    private AudioSource ungrab_source;

    private float sound_timestamp ;
    public float delay_footstep = 0.2f ;


    void Start()
    {
        grabbing = false;
        minimum_speed = 0.05f;
        cc = gameObject.GetComponent<CharacterController>();
        m_Animator = GameObject.Find("convexe_action").GetComponent<Animator>();
        GameObject audio_steps = GameObject.Find("AudioSteps");
        audio_sources = audio_steps.GetComponentsInChildren<AudioSource>();
        sound_timestamp = Time.time;

        grab_source = GameObject.Find("Sound_Grab").GetComponent<AudioSource>();
        ungrab_source = GameObject.Find("Sound_Ungrab").GetComponent<AudioSource>();

    }

    void Update()
    {
        bool isP = Input.GetButton("grab_p"+player_id);
        if (isP && !grab_button) {            
            m_Animator.SetBool("isGrabbing", true);
            grab_source.Play();

        } else if (!isP && grab_button) {           
            m_Animator.SetBool("isGrabbing", false);
            ungrab_source.Play();
        }
        grabbing = isP;
        grab_button = isP;
        if (grabbing == false) {
            movement();            
        }
    }

    void movement() {
        var speed = (Mathf.Abs(Input.GetAxis ("front_p"+player_id)) + Mathf.Abs(Input.GetAxis ("side_p"+player_id))) / 2.0f;
        if (speed > minimum_speed)
        {
            float joyY = Input.GetAxis("front_p" + player_id);
            float joyX = Input.GetAxis("side_p" + player_id);

            Quaternion rot = Quaternion.AngleAxis(get_joy_angle(), Vector3.up);
            transform.rotation = rot;

            Vector3 direction = new Vector3(joyX, 0.0f, joyY);
            cc.Move(new Vector3(joyX, 0.0f, joyY) * sensibility);
            m_Animator.SetBool("isWalking", true);
            play_footstep(delay_footstep / direction.magnitude);
        }
        else {
            m_Animator.SetBool("isWalking", false);
        }
            
    }

    // void OnTriggerEnter(Collider collider)
    // {
    //     //Output the Collider's GameObject's name
    //     Debug.Log(collider.name);
    // }

    void OnTriggerStay(Collider collider) {
        if (collider.tag == "Mirror" && grab_button)
        {
            float joyX = -Input.GetAxis ("side_p"+player_id);

            if (Mathf.Abs(joyX) > 0.2f) {
                grabbing = true;
                collider.gameObject.transform.Rotate(Vector3.up * joyX * sensibility * 4.0f);
                Vector3 mirror_center =  collider.gameObject.transform.position;
                transform.LookAt(mirror_center);
                gameObject.transform.RotateAround(mirror_center, Vector3.up, joyX * sensibility * 4.0f);

                collider.gameObject.GetComponent<mirrorManager>().play_sound();
                m_Animator.SetBool("isGrabbing", true);              
            } else {
                collider.gameObject.GetComponent<mirrorManager>().pause_sound();
            }
        } else if (collider.tag == "MirrorRail" && grab_button)
        {
            grabbing = true;
            float joyX = Input.GetAxis ("side_p"+player_id);
            Vector3 direction = new Vector3(Input.GetAxis ("front_p"+player_id), 0, Input.GetAxis ("side_p"+player_id));
            Vector3 cross = Vector3.Cross(direction, collider.GetComponent<RailMirror>().direction);
            float angle = joyX;
            // angle = 1.0f - angle;
            // Debug.Log(cross);
            bool ret = collider.GetComponent<RailMirror>().Move(collider.GetComponent<RailMirror>().direction * sensibility * angle);
            // collider.transform.Translate(collider.GetComponent<RailMirror>().direction * sensibility * angle, Space.World);
            // Vector3 mirror_center =  collider.gameObject.transform.position;
            // gameObject.transform.RotateAround(mirror_center, Vector3.up, joyX * sensibility * 4.0f);
            if(ret) {                
                cc.Move(collider.GetComponent<RailMirror>().direction * sensibility * angle);
            }
              
        } else if (grab_button == false ) {
            grabbing = false;
            m_Animator.SetBool("isGrabbing", false);
            if (collider.tag == "Mirror") {
                collider.gameObject.GetComponent<mirrorManager>().pause_sound();
            }
        }
    }

    private float get_joy_angle() {
        float joyY =  Input.GetAxis ("front_p"+player_id);
        float joyX = Input.GetAxis ("side_p"+player_id);
        var angle = Mathf.Atan2(joyY, -joyX);
        return Mathf.Rad2Deg * angle - 90;       
    }


    private void play_footstep(float delay) {
        if (Time.time > (sound_timestamp + delay)) {
            AudioSource playing_sound =  audio_sources[Random.Range(0, 6)];
            playing_sound.pitch = Random.Range(0.9f, 1.1f);
            playing_sound.Play();
            sound_timestamp = Time.time;
        }
       
    }

}
