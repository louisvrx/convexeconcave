﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailMirror : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 direction = new Vector3(0,0,1.0f);
    public float minValue = -2.0f;
    public float maxValue = 2.0f;

    private Vector3 startPosition;
    void Start()
    {
        startPosition = transform.position;
    }
    
    public bool Move(Vector3 vector){
        Vector3 distance = (transform.position + vector) - startPosition;
        if(distance.x > minValue && distance.x < maxValue)
        {
            transform.Translate(vector, Space.World);
            return true;
        }
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
