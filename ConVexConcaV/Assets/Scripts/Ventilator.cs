﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ventilator : MonoBehaviour
{
    private Transform m_Ventilator;
    private Vector3 m_Rotation;
    // Start is called before the first frame update
    void Start()
    {
        m_Ventilator = this.transform;
        m_Rotation = m_Ventilator.localEulerAngles;
    }   

    // Update is called once per frame
    void FixedUpdate()
    {

        m_Rotation.y += Time.fixedDeltaTime*100;
        m_Ventilator.localEulerAngles = new Vector3(
            m_Ventilator.localEulerAngles.x,
            m_Rotation.y,
            m_Ventilator.localEulerAngles.z
            );
    }
}
