﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mirrorManager : MonoBehaviour
{
    private AudioSource audio_source;
    private float max_volume;
    private float step_fade = 0.01f;
    private float delay_fade = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        audio_source = gameObject.GetComponent<AudioSource>();
        max_volume = audio_source.volume;
    }


    public void play_sound(){
        if (!audio_source.isPlaying) {
                StartCoroutine("FadeIn");
                audio_source.Play();
        }
    }

    public void pause_sound() {
        StartCoroutine("FadeOut");
        audio_source.Pause();
    }

    public IEnumerator FadeIn() {
         for (float vol = audio_source.volume ; vol < max_volume ; vol = vol+step_fade) {
                    audio_source.volume = vol;
                }
        yield return new WaitForSeconds(delay_fade);
    }

    public IEnumerator FadeOut() {
         for (float vol = audio_source.volume ; vol >= 0.0f ; vol = vol-step_fade) {
                    audio_source.volume = vol;
                }
        yield return new WaitForSeconds(delay_fade);
    }

}
